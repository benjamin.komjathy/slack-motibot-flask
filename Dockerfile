#FROM tiangolo/uvicorn-gunicorn-fastapi:python3.9-slim

#COPY ./app /app

FROM python:3.9.4-slim

# set work directory
WORKDIR /app

# set env variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1


# install dependencies
COPY requirements.txt .
RUN pip install -r requirements.txt

# copy project
COPY ./src .

CMD python /app/flask-slack.py
