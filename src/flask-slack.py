#!/usr/bin/env python3
from __future__ import print_function

from flask import Flask, redirect, url_for, render_template, request, send_file
import subprocess
from dotenv import load_dotenv
import json, os
from urllib import parse as urlparse
import requests
import xml.etree.ElementTree as ET
import requests



app = Flask(__name__)

load_dotenv()

citatum_user=os.environ['CITATUM_USER']
citatum_pw=os.environ['CITATUM_PW']

@app.route("/", methods = ['POST'])
def index():
    print(request.form)
    return lambda_handler(request.form['command'], request.form['text'])

def idezetteszt(kat):
    if kat == "sp":
        r = requests.get(
            f'https://api.citatum.hu/idezet.php?f={citatum_user}&j={citatum_pw}&szerzo=szabo+peter&rendez=veletlen')
        root = ET.fromstring(r.content)
        idezet = root[0][0].text + " - A Mester Maga"
    elif kat == "pc":
        r = requests.get(
            f'https://api.citatum.hu/idezet.php?f={citatum_user}&j={citatum_pw}&szerzo=paulo+coelho&rendez=veletlen')
        root = ET.fromstring(r.content)
        idezet = root[0][0].text + " - Paulo Coelho"
    elif kat == "en":
        response = requests.get("https://zenquotes.io/api/random")
        json_data = json.loads(response.text)
        idezet = json_data[0]['q'] + " -" + json_data[0]['a']
    elif kat == "help":
        idezet = "kategoriak-> https://www.citatum.hu/kategoria/ ,az 'sp' kategória megadásával Magától a Mestertől kaphatsz idézetet, az 'en' kategoriával pedig angolul"

    else:
        r = requests.get(
            f'https://api.citatum.hu/idezet.php?f={citatum_user}&j={citatum_pw}&kat={kat}&rendez=veletlen')
        root = ET.fromstring(r.content)
        try:
            idezet = root[0][0].text + " - " + root[0][1].text
        except:
            idezet = "Nincs ilyen kategória"

    return (idezet)

def oktatas(dummy):
    return "https://devopsakademia.com/courses/"


commands = {'idezet': idezetteszt, 'oktatas': oktatas}  # map of command aliases


def lambda_handler(command, text):
    
    
    params = text.split()
    subcommand = params[0].lower()
    if (len(params) < 2):
        response = f'available subcommands: {list(commands.keys())} + 1 parameter'
    elif (subcommand in commands.keys()):
        response = f'{subcommand} ide valami hiba response' if len(params) < 2 else f'{commands[subcommand](params[1])}'
    else:
        response = f'illegal sub command >{subcommand}<, commands available {list(commands.keys())}'

    # logging
    print (str(command) + ' ' + str(params) +' -> '+ response )

    return {
        "response_type": "in_channel",
        "text": ' ' + " ".join(params),
        "attachments": [
            {
                "text": response
            }
        ]
    }


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)