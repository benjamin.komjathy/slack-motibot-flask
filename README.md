# Slackbot with Flask

- [Slackbot with Flask](#slackbot-with-flask)
  - [How to Run](#how-to-run)
  - [Ngrok](#ngrok)
    - [Be aware of bots](#be-aware-of-bots)
  - [Slack](#slack)


## How to Run

```Dockerfile
#Dockerfile
FROM python:3.9.4-slim

# set work directory
WORKDIR /app

# set env variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1


# install dependencies
COPY requirements-flask.txt .
RUN pip install -r requirements-flask.txt

# copy project
COPY . .

CMD python /app/src/flask-slack.py
```

```yaml
#docker-compose.yml
version: '3.2'

services:
  web:
    build:
      context: .
      dockerfile: Dockerfile
    image: flask-moti
    container_name: flask-moti
    env_file:
      - .env
    volumes:
      - .:/app
    ports:
      - 5000:5000
```

```sh
docker-compose up -d --build
```

```
curl "localhost:5000" -d "command=%2Fmoti&text=idezet+elet"
```

```sh
docker logs flask-moti -f
```



## Ngrok
Selfhost without any portforward or reverseproxy
```sh
curl -s https://ngrok-agent.s3.amazonaws.com/ngrok.asc | sudo tee /etc/apt/trusted.gpg.d/ngrok.asc >/dev/null && echo "deb https://ngrok-agent.s3.amazonaws.com buster main" | sudo tee /etc/apt/sources.list.d/ngrok.list && sudo apt update && sudo apt install ngrok
```

```sh
ngrok config add-authtoken <token>
```

```sh
ngrok http 5000
```

```
curl "https://3cdb-2001-4c4e-1319-3a00-5ffd-c8f4-4ad2-7f9c.ngrok-free.app/" -d "command=%2Fmoti&text=idezet+elet"
```

### Be aware of bots

![ngrokbots.png](ngrokbots.png)

## Slack

[https://api.slack.com/apps](https://api.slack.com/apps)

Setup slash slashcommand, install to workspace

```
/moti idezet elet
```